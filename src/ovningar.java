import java.util.Scanner;

public class ovningar {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {


        //Här skapar jag en while loop
        boolean isRunning = true;
        while (isRunning) {

            System.out.println("Write a text");
            //Jag sätter värdet som användaren skriver in till input av typen String
            String input = scanner.nextLine();

            //Här har jag en if och else sats där värdet som användaren skriver in inte får vara en tom sträng
            //vilket då gör att programmet stängs av
            if (input != ""){
                //Här sparar jag värdet i en variabel
                String text = input;
                System.out.println(text);
            } else {
                isRunning = false;
            }



        }
    }
}
